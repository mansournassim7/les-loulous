import datetime

from pydantic import BaseModel


class User():
    def __init__(self, id: int, nom: str, prenom: str, date_de_naissance: datetime.date, adresse: str, telephone: str,
                 profil: str, email: str):
        self.id = id
        self.nom = nom
        self.prenom = prenom
        self.date_de_naissance = date_de_naissance
        self.adresse = adresse
        self.telephone = telephone
        self.profil = profil
        self.email = email


class UserInput(BaseModel):
    id: int = None
    nom: str = None
    prenom: str = None
    date_de_naissance: str = None
    adresse: str = None
    telephone: str = None
    profil: str = None
    email: str = None
