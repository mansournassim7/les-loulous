from fastapi import testclient
from main import app

app = app

client = testclient.TestClient(app)


def test_read_main():
    response = client.get("/")

    assert response.status_code == 200
    assert response.json() == {"Hello": "World !"}


def test_new_user_successful_1():
    response = client.post(url="/new_user/", json={
        "id": 1,
        "nom": "Janus",
        "prenom": "Hugh",
        "date_de_naissance": "05/02/2002",
        "adresse": "87, impasse de Guillou",
        "telephone": "56204",
        "profil": "Professeur",
        "email": "hugh.janus@protonmail.com"
    })

    assert response.status_code == 200
    assert response.json() == {"detail": "Utilisateur ajouté avec succès !"}


def test_new_user_successful_2():
    response = client.post(url="/new_user/", json={
        "id": 6,
        "nom": "Doe",
        "prenom": "John",
        "date_de_naissance": "03/09/1996",
        "adresse": "17 rue de Lille",
        "telephone": "0766008523",
        "profil": "Parent",
        "email": "john.doe@gmail.com"
    })

    assert response.status_code == 200
    assert response.json() == {"detail": "Utilisateur ajouté avec succès !"}


def test_new_user_failed_1():
    response = client.post(url="/new_user/", json={
        "id": 2,
        "nom": "Oxlong",
        "prenom": "Mike",
        "date_de_naissance": "01/01/1999",
        "adresse": "52, rue de Leroux",
        "telephone": "1208",
        "profil": "Data Scientist",
        "email": "mike.oxlong@gmail.com"
    })

    assert response.status_code == 500
    assert response.json() == {"detail": "Le profil de l'utilisateur n'existe pas."}


def test_new_user_failed_2():
    response = client.post(url="/new_user/", json={
        "id": 3,
        "nom": "Mama",
        "prenom": "Joe",
        "date_de_naissance": "1991/05/02",
        "adresse": "650, boulevard Raymond Gomes",
        "telephone": "31649",
        "profil": "Parent",
        "email": "joe.mama@gmail.com"
    })

    assert response.status_code == 500
    assert response.json() == {"detail": "La date de naissance de l'utilisateur n'est pas conforme."}


def test_new_user_failed_3():
    response = client.post(url="/new_user/", json={
        "id": -4,
        "nom": "OULD ALI",
        "prenom": "Walid",
        "date_de_naissance": "03/05/2007",
        "adresse": "1, rue Rémy Michaud",
        "telephone": "15461",
        "profil": "Eleve",
        "email": "walid.le.bg@gmail.com"
    })

    assert response.status_code == 500
    assert response.json() == {"detail": "L'id de l'utilisateur n'est pas conforme."}


def test_new_user_failed_4():
    response = client.post(url="/new_user/", json={
        "id": 5,
        "nom": "Dubois",
        "prenom": "Bernard",
        "date_de_naissance": "02/03/2002",
        "adresse": "18, Boulevard de l'indépendance",
        "telephone": "8808965",
        "profil": "Parent",
        "email": "duber.outlook.fr"
    })

    assert response.status_code == 500
    assert response.json() == {"detail": "L'adresse email de l'utilisateur n'est pas conforme."}
