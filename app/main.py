import datetime

from fastapi import FastAPI, HTTPException
from user import UserInput, User
import re

fake_database = []

profils = ["Parent", "Eleve", "Professeur"]
app = FastAPI()


@app.get("/")
def hello_world():
    return {"Hello": "World !"}


@app.post("/new_user/")
def new_user(user: UserInput):
    dic = user.__dict__
    if type(dic["id"]) is not int or dic["id"] <= 0:
        raise HTTPException(status_code=500, detail="L'id de l'utilisateur n'est pas conforme.")
    if type(dic["nom"]) is not str or len(dic["nom"]) < 2:
        raise HTTPException(status_code=500, detail="Le nom de l'utilisateur n'est pas conforme.")
    if type(dic["prenom"]) is not str or len(dic["prenom"]) < 2:
        raise HTTPException(status_code=500, detail="Le prenom de l'utilisateur n'est pas conforme.")

    pattern = re.compile(
        r"""^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[13-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$""")
    if not pattern.match(dic["date_de_naissance"]):
        raise HTTPException(status_code=500, detail="La date de naissance de l'utilisateur n'est pas conforme.")
    pattern_email_regex = re.compile(r"^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$")
    if type(dic["email"]) is not str or not pattern_email_regex.match(dic["email"]):
        raise HTTPException(status_code=500, detail="L'adresse email de l'utilisateur n'est pas conforme.")
    if type(dic["adresse"]) is not str:
        raise HTTPException(status_code=500, detail="L'adresse de l'utilisateur n'est pas conforme.")
    if type(dic["telephone"]) is not str:
        raise HTTPException(status_code=500, detail="Le numéro de téléphone de l'utilisateur n'est pas conforme.")
    if type(dic["profil"]) is not str or dic["profil"] not in profils:
        raise HTTPException(status_code=500, detail="Le profil de l'utilisateur n'existe pas.")

    fake_database.append(
        User(dic["id"],
             dic["nom"],
             dic["prenom"],
             datetime.datetime.strptime(dic["date_de_naissance"], "%d/%m/%Y"),
             dic["adresse"],
             dic["telephone"],
             dic["profil"],
             dic["email"]))

    return {"detail": "Utilisateur ajouté avec succès !"}
