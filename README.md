# Méthodologie de développement d'applications

## Vision Produit de notre projet

- **POUR :** les enfants ayant besoin de soutien scolaire
- **QUI SOUHAITENT :** offrir des cours aux enfants par des étudiants.
- **NOTRE PRODUIT :** est un site web qui permet de mettre en relation qui veulent vendre des cours et des enfants qui
  veulent acheter des cours.
- **QUI :** permet aux étudiants qui n'ont pas beaucoup de revenus d'arrondir leur fin de mois et aux enfants qui ont
  des parents n'ayant pas beaucoup de moyens financiers de bénéficier de cours abordables.
- **A LA DIFFÉRENCE DE :** SuperProf, DigiSchool
- **PERMET DE :** mettre en relation des étudiants qui ont besoin d'argent et des parents qui veulent des cours de
  soutien pour leur enfants à des prix abordables

## Tâches

- A : Conception graphique (Interface pour le Cadre de réponses parents, les étudiants et les élèves)
- B : Formation de l'équipe dev. à Node.js et JavaScript
- C : Développement du site en HTML et CSS
- D : Intégration d'une BDD au site (PostgreSQL)
- E : Référencement
- F : Choix du nom de domaine et hébergement
- G : Sécuriser son site web avec HTTPS

### Estimation des durées des tâches

|tâche|durée|prédecesseur|
|---|---|---|
|A|7|\-|
|B|21|\-|
|C|5|A|Cadre de réponse
|D|20|B, C|
|E|1|D|
|F|1|E|
|G|1|F|  Cadre de réponse

### Diagramme de PERT

[PERT](https://drive.google.com/file/d/15bqAYAOPT3aT0BjgWufv9NyUbVaZnTw4/view?ts=6140c25e)

# Cahier des charges

## Présentation générale du problème

### Projet

#### Finalités et espérance de retour sur investissement

Si le produit marche bien, on baisse un peu le prix et on améliore le site afin d'attirer plus de clients.

### Contexte

Dans le contexte de soutien scolaire, notre produit est un site Web qui permet aux étudiants de proposer des cours aux
élèves de primaire, collège et lycée.

### Énoncé du besoin

Notre projet consiste à créer un site web de cours de soutien en ligne destiné aux élèves avec peu de moyens financiers
ayant besoin de soutien scolaire. Il permet d'autre part aux étudiants universitaires d'arrondir leur fin de mois en
proposant des cours de soutien à ces enfants sur notre plateforme. A la différence d'OpenClassrooms qui emploient des
enseignants et qui rédigent eux mêmes les cours. Par ailleurs, cela permet aux étudiants d'acquérir de l'expérience en
enseignant à ces élèves.

### Environnement du produit recherché

#### Éléments

- **Personnes** : les **étudiants** qui vont enseigner aux **élèves** des **parents**.
- **Équipement** : les élèves devront se munir d'un ordinateur ainsi que d'une connexion portable fiable.

#### Contraintes

- Le site doit protéger les données des utilisateurs.
- Il doit aussi mettre en place une solution permettant de sécuriser le paiement des cours en ligne.

## Expression fonctionnelle du besoin

### Fonctions de service et de contrainte

- A : Lecture des cours des professeurs sous format vidéo et écrit, par les élèves.  
  *CA - Les vidéos peuvent être visionnées et les cours consultés via la plateforme pour les élèves abonnés*
- B : Consultation des profils des professeurs par les élèves et parents.  
  *CA - Les profils sont consultables par les élèves et les parents.*
- C : Modification des profils des professeurs par les professeurs.  
  *CA - Les profils sont modifiables par les professeurs*
- D : Visionner et diffuser les cours en direct et système de chat.  
  *CA - Les directs peuvent être visionnés via la plateforme pour les élèves abonnés et système de chat*
- E : Achat des cours par les parents.  
  *CA - Les parents doivent pouvoir achetés les cours mis en ligne par les professeurs*

## Cadre de réponse

### Solution proposée

- A : Support du CODEC sur Chrome, Edge, Firefox, Opera et Safari. Les vidéos seront stockés sous forme de fichiers .mkv
  dans nos bases de données PostgreSQL.
- B : Les pages PHP des professeurs sont demandées à nos serveurs et sont envoyées par ceux-ci.
- C : Les données des profils des professeurs seront stockés dans notre base de données et lorsqu'il est modifié cela
  met à jour la base de données
- D : Lecteur pour visionner et diffuser les vidéos sur Chrome, Edge, Firefox, Opera et Safari. Utilisation du chat
  Twitch.
- E : Paiement par PayPal, carte bleue et paysafecard.

## Pour l'ensemble du produit

### Prix de la réalisation de la version de base

- On compte prendre trois stagiaires qui sont en bac +3/+4 qui souhaitent effectuer un stage rémunéré de 10 semaines en
  entreprise
- 44 Jours 3 stagiaires que nous paierons 50€ la journée -> 3 * 50 * 44 = 6 600€
- prêt d'un ordinateur portable aux trois stagiaires -> MacBook Air 2020, 1200€ chacun -> 3 600€
- Hébergement + nom de domaine chez Hostinger + sécurisation du site web avec HTTPS : 9.99€/mois -> 119.88€ par an

| Charges | Coûts |
| ------ | ------ |
| 3 stagiaires | 6 600 € |
| 3 ordinateurs portable | 3 600 € |
| Hébergement, nom de domaine + sécurisation  | 119.88 € |
| Hébergement, nom de domaine + sécurisation  | 10 319.88 € |

### Contraintes

- Sécurité et respect de la vie privée et des données des utilisateurs
- Entretien de la base de données: suppression des anciens utilsateurs et éviter les redondances
- Choisir un bon service d'hébergement (AWS, ovh)

## Perspective d'évolution technologique

- Redesign du site web éventuel
- Développement d'une application mobile

# Estimation des coûts pour le projet

- On compte prendre trois stagiaires qui sont en bac +3/+4 qui souhaitent effectuer un stage rémunéré de 10 semaines en
  entreprise
- 44 Jours 3 stagiaires que nous paierons 50€ la journée -> 3 * 50 * 44 = 6 600€
- prêt d'un ordinateur portable aux trois stagiaires -> MacBook Air 2020, 1200€ chacun -> 3 600€
- Hébergement + nom de domaine chez Hostinger + sécurisation du site web avec HTTPS : 9.99€/mois -> 119.88€ par an

| Charges | Coûts |
| ------ | ------ |
| 3 stagiaires | 6 600 € |
| 3 ordinateurs portable | 3 600 € |
| Hébergement, nom de domaine + sécurisation  | 119.88 € |
| Hébergement, nom de domaine + sécurisation  | 10 319.88 € |

# Backlog

## User Stories

Utilisateurs :

- Élève
- Professeur
- Étudiant

### Eleves

En tant qu'élève je veux pouvoir :

- Commenter dans l'espace commentaire afin que les autres utilisateurs puissent voir mes commentaires.
- Poster des questions sur un chat afin que les professeurs puissent les voir durant le direct.

### Professeurs

En tant que professeur je veux pouvoir :

- Éditer mon profil, changer la photo de profil, sa description, ses informations personnelles, depuis mon profil afin
  que les élèves et parents puissent voir mon profil
- Je veux pouvoir, depuis une interface de gestion de mes cours, les modifier, ajuster le prix afin que les parents
  puissent les acheter et les voir.
- Uploader des fichiers vidéos, des images et éditer mon cours afin que les élèves puissent y accéder

### Parents

En tant que parent je veux pouvoir :

- Acheter les cours afin que mon enfant élève puisse le voir
- Noter les profs et les cours afin que les admins puissent contrôler la qualité des cours

### Admins

En tant que modérateurs je veux pouvoir :

- Contrôler le contenu des cours des professeurs afin de vérifier que le contenu respectent les TOS (Terms of services)

## Technical Stories

- En tant que développeur web, je dois pouvoir maîtriser des langages / outils de développement web (NodeJs pour le
  backend, JavaScript et ses frameworks pour le front-end)

## Capacité totale : 57 points

## Capacité initiale : 22

## Durée d'un sprint : 2 semaines, au total, on fera 3 sprints

## Durée des projets : (57 * 2) / 19 = 6 semaines = 45 jours

## Matrice de tests

[Matrice de tests](https://docs.google.com/spreadsheets/d/1QWI3Ghnoz5b8ilnEPZKo2R3BarNcE56HAmrXN6DU3dI/edit?usp=sharing)

|                  ID                  |             1             |           2           |               3              |           -4          |                5                |          6         |
|:------------------------------------:|:-------------------------:|:---------------------:|:----------------------------:|:---------------------:|:-------------------------------:|:------------------:|
|                  Nom                 |           Janus           |         Oxlong        |             Mama             |        OULD ALI       |              Dubois             |         Doe        |
|                Prenom                |            Hugh           |          Mike         |              Joe             |         Walid         |             Bernard             |        John        |
|           Date de naissance          |         05/02/2002        |       01/01/1999      |          1991/05/02          |       03/05/2007      |            02/03/2002           |     03/09/1996     |
|                 Email                | hugh.janus@protonmail.com | mike.oxlong@gmail.com |      joe.mama@gmail.com      | walid.le.bg@gmail.com |         duber.outlook.fr        | john.doe@gmail.com |
|                Adresse               |   87, impasse de Guillou  |   52, rue de Leroux   | 650, boulevard Raymond Gomes |  1, rue Rémy Michaud  | 18, Boulevard de l'indépendance |   17 rue de Lille  |
|               Téléphone              |           56204           |          1208         |             31649            |         15461         |             8808965             |      766008523     |
|                Profil                |         Professeur        |     Data Scientist    |            Parent            |         Eleve         |              Parent             |       Parent       |
|                                      |                           |                       |                              |                       |                                 |                    |
|       ID est un entier positif       |            VRAI           |          VRAI         |             VRAI             |          FAUX         |               VRAI              |        VRAI        |
|           Nom est conforme           |            VRAI           |          VRAI         |             VRAI             |          VRAI         |               VRAI              |        VRAI        |
|          Prénom est conforme         |            VRAI           |          VRAI         |             VRAI             |          VRAI         |               VRAI              |        VRAI        |
| Date de naissance est au bon format  |            VRAI           |          VRAI         |             FAUX             |          VRAI         |               VRAI              |        VRAI        |
|          Email est conforme          |            VRAI           |          VRAI         |             VRAI             |          VRAI         |               FAUX              |        VRAI        |
|         Adresse est conforme         |            VRAI           |          VRAI         |             VRAI             |          VRAI         |               VRAI              |        VRAI        |
|        Téléphone est conforme        |            VRAI           |          VRAI         |             VRAI             |          VRAI         |               VRAI              |        VRAI        |
|          Profil est conforme         |            VRAI           |          FAUX         |             VRAI             |          VRAI         |               VRAI              |        VRAI        |
|                                      |                           |                       |                              |                       |                                 |                    |
|          Utilisateur Inséré          |            OUI            |       Exception       |           Exception          |       Exception       |            Exception            |         OUI        |

## Tests unitaires

Nous avons décidé d'utiliser le framework `pytest` pour les tests.

Pour lancer les tests, situez-vous à la racine de ce projet et exécutez cette commande :

```shell
pytest-3 app/test.py
```

Vous aurez alors l'affichage :

![](doc/tests.png)

## Qualité Logiciel

Après avoir utilisé le logiciel SonarQube, nous avons eu les résultats suivants :

### Résultat 1

![](doc/SonarQube_1.png)

### Résultat 2

![](doc/SonarQube_2.png)

## Tests d'intégration

Afin que les tests d'intégration puissent s'exécuter chez vous, vous devez d'abord installer PostgreSQL.

Pour lancer les tests d'intégration, situez-vous à la racine de ce projet et exécutez cette commande :

```shell
pytest-3 app/integration_tests.py
```

Vous aurez alors l'affichage :

![](doc/integration_tests.png)

Une fois les tests d'intégration terminés, on peut vérifier que les deux utilisateurs insérés aux tests d'intégration
ont bien été créés :

![](doc/resultats_integration.png)

## Pipeline d'intégration continue
Nous avons ajouté un script `.gitlab-ci.yml` qui lance les tests automatiquement à chaque `push` sur la branche `main`.

Résultat obtenu :  

![](doc/job_succeeded.png)