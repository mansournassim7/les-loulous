apt update
apt install -y postgresql postgresql-contrib
apt install -y systemctl
systemctl start postgresql.service
pg_ctlcluster 13 main start
apt-get install sudo -y
sudo -i -u postgres psql -c "CREATE ROLE root SUPERUSER"
sudo -i -u postgres psql -c "ALTER ROLE root WITH login"
pip install -U pytest
pip install psycopg
pip install --no-cache-dir --upgrade -r requirements.txt